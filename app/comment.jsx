//File structure


// - CommentBox
//    - CommentList
//      - Comment
//    - CommentForm


import React from 'react';

//Variables






//tier 1 components
export default class CommentBox extends React.Component {

  render() {

    return (
      <div className="commentBox">
        <h1>Comments</h1>
        <CommentList data={this.props.data}/>
        <CommentForm/>
      </div>
    );
  }

};

//tier 2 components

class CommentList extends React.Component {
  render() {
      return(
        <div>
        { this.getCommentNodes() }
        </div>
      )
  }

  getCommentNodes(){
    return this.props.data.map(function (comment){
      return (
      <Comment author={comment.author}>
      {comment.text}
      </Comment>
      )
    });
  }

};





  class CommentForm extends React.Component {
    render() {
      return (
      <div className="commentForm">
        I am a CommentForm.
      </div>
    );
    }
  }
//tier3 components


class Comment extends React.Component {
  render() {
    return (
      <div className='comment'>
        <h2 className="commentAuthor">
        {this.props.author}
        </h2>
        {this.props.children}
      </div>
    );
  }
}
