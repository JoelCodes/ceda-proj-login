import React from 'react'


/////////////////////////////////////////Design
//LoginBox
  //Form
  //Form


export default class LoginPage extends React.Component {
constructor() {
super();

}




  render() {
    return (
      <div id="LoginBox" className="row z-depth-4" style={this.loginPageStyles()}>
        <LoginForm />
      </div>
    )
  }

  loginPageStyles() {
    return {
      backgroundColor: 'rgba(255,215,224,0.3)',
      margin: '20% auto',
      width: '50%',
      padding: '0',
      borderRadius: '5px',
      position: 'relative'
    }
  }
}


class LoginForm extends React.Component {
  render() {
    return(
      <form className="col s12">
        <div className="row">
          <div className="input-field col s12">
            <h5>Username</h5>
            <input type="text" name="Username"></input>
          </div>
        </div>
        <div className="row">
          <div className="input-field col s12">
            <h5>Password</h5>
            <input type="text" name="Password"></input>
          </div>
        </div>
        <div className='row'>
          <div className="col s12 ">
            <button style={this.buttonCss()} className="btn btn-large  waves-effect waves-light-blue" type="submit" name="action">Submit
            </button>
          </div>

        </div>
      </form>

    )
  }


    buttonCss() {
      return {

      }
    }
}
