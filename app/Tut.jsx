import React from 'react';


export default class Main extends React.Component {
  constructor() {
    super();
    this.handleClick=this.handleClick.bind(this);
    this.state = {counter: 0}
  }


  render() {
    return (
      <div>
        <But localHandleClick={this.handleClick} increment={1} />
        <But localHandleClick={this.handleClick} increment={5} />
        <But localHandleClick={this.handleClick} increment={10} />
        <But localHandleClick={this.handleClick} increment={20} />
        <But localHandleClick={this.handleClick} increment={100} />
        <But localHandleClick={this.handleClick} increment={1000} />

        <Result localCounter={this.state.counter} />
      </div>
    )
  }


  handleClick(increment) {

    this.setState({counter: this.state.counter + increment });

  }


}


class But extends React.Component {
  constructor() {
    super();
    this.localHandleClick = this.localHandleClick.bind(this)
  }
  render() {
    return(
      <button style={this.butCss()} onClick={this.localHandleClick}>+{this.props.increment} </button>

    );
  }

  localHandleClick() {
    this.props.localHandleClick(this.props.increment);
  }

  butCss() {
    if (this.props.increment % 2 == 0) {
    return {
        backgroundColor: 'rgba(255,215,224,0.3)'
    }
  } else {
    return {
        backgroundColor: 'rgba(144,215,0,0.3)'
    }
  }
  }


};

class Result extends React.Component {
  render() {
    return (
      <div>
        {this.props.localCounter}
      </div>
    );
  }
}
